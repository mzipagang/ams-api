const chai = require('chai');
const chaiArrays = require('chai-arrays');
const sinonChai = require('sinon-chai');

chai.use(chaiArrays);
chai.use(sinonChai);

before(async () => {
  console.log('**** UNIT TESTS, OVERRIDING ENV VARS ****'); // eslint-disable-line no-console
});
