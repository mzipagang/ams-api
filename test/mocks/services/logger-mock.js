module.exports = () => ({
  error: () => { },
  warn: () => { },
  info: () => { },
  verbose: () => { },
  debug: () => { },
  silly: () => { },
  accessLogger: (req, res, next) => next()
});
