const Promise = require('bluebird');
const express = require('express');

module.exports = () => {
  const app = express();
  const router = express.Router();

  return {
    app,
    router,
    start: (routes) => {
      if (routes) {
        router.use(routes);
      }

      return Promise.try(() => { });
    }
  };
};
