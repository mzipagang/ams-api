const Promise = require('bluebird');

const dataWorkerRequest = () => Promise.try(() => ({
  statusCode: '200',
  body: {
    data: {
    }
  }
}));

module.exports = (method, url, body, headers) => dataWorkerRequest(method, url, body, headers);
module.exports.get = dataWorkerRequest.bind(null, 'get');
module.exports.post = dataWorkerRequest.bind(null, 'post');
module.exports.put = dataWorkerRequest.bind(null, 'put');
module.exports.patch = dataWorkerRequest.bind(null, 'patch');
module.exports.delete = dataWorkerRequest.bind(null, 'delete');
module.exports.options = dataWorkerRequest.bind(null, 'options');
module.exports.head = dataWorkerRequest.bind(null, 'head');
