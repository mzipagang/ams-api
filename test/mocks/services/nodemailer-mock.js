let inputConfig;
const transport = {
  sendMail: (options, cb) => {
    cb(undefined, { status: 200, options, config: inputConfig });
  }
};

module.exports = () => ({
  createTransport: (config) => {
    inputConfig = config;
    return transport;
  }
});
