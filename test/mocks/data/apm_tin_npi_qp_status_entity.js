
module.exports = () => ({
  apm_patient_count_denominator: 'XXXXXXXXXX',
  apm_patient_count_numerator: 'XXXXXXXXXX',
  apm_patient_threshold_met: 'Y',
  apm_patient_threshold_score: 'XXX.XX',
  apm_payment_denominator: 'XXXXXXXXXX',
  apm_payment_numerator: 'XXXXXXXXXX',
  apm_payment_threshold_met: 'Y',
  npi: 'XXXXXXXXXX',
  organization_id: 'XXXXXXXXXX',
  pq_year: 'XXXX',
  program_id: 'XXX',
  qp_reporting_period: 'XXXXXXXXXX',
  qp_status: 'Y',
  run_date: '2017-01-01',
  run_number: 'XX',
  subdiv: 'XX',
  tin: 'XXXXXXXXX'
});
