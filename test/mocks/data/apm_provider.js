const moment = require('moment');

module.exports = () => ({
  entity_id: '0000-0000-0000-0000',
  start_date: moment.utc()
    .add(-1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  end_date: moment.utc()
    .add(1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  tin: 'XX-XXXXXXX',
  npi: 'XXXXXXXXXX',
  ccn: 'ccn',
  tin_type_code: 'XXX-XX-XXXX',
  specialty_code: '',
  organization_name: '',
  first_name: '',
  middle_name: '',
  last_name: '',
  participant_type_code: ''
});
