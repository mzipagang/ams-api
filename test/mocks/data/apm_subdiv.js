const moment = require('moment');

module.exports = () => ({
  id: '0000-0000-0000-0000',
  apm_id: '0000-0000-0000-0000',
  name: 'Semantic Health',
  start_date: moment.utc()
    .add(-1, 'month')
    .startOf('month')
    .startOf('day')
    .format('YYYY-MM-DD'),
  end_date: moment.utc()
    .add(1, 'month')
    .startOf('month')
    .startOf('day')
    .format('YYYY-MM-DD'),
  advanced_apm_flag: true,
  mips_special_scoring_flag: 'Y'
});
