module.exports = data => data || {
  entity_id: '12345',
  npi: '12345',
  tin: '123456789',
  start_date: '2017-01-01T00:00:00.000Z',
  end_date: '9999-12-31T00:00:00.000Z',
  ccn: '',
  entity_data: {
    id: 'AA-AAAAAA',
    apm_id: '1',
    subdiv_id: '',
    start_date: '2017-01-01T00:00:00.000Z',
    end_date: '9999-12-31T00:00:00.000Z',
    name: '',
    dba: '',
    tin: '123456789',
    npi: '12345',
    additional_information: '',
    type: '',
    address: {
      state: '',
      zip4: '',
      zip5: '',
      city: '',
      address_2: '',
      address_1: ''
    }
  },
  model_data: {
    id: '1',
    name: 'Test model',
    short_name: 'Test',
    start_date: '2017-01-01T00:00:00.000Z',
    end_date: '2021-12-31T00:00:00.000Z',
    advanced_apm_flag: '',
    mips_special_scoring_flag: '',
    apm_quality_reporting_category: ''
  },
  subdiv_data: [
    {
      apm_id: '1',
      id: '1',
      name: 'Test Subdiv',
      start_date: '2017-01-01T00:00:00.000Z',
      end_date: '2021-12-31T00:00:00.000Z',
      advanced_apm_flag: '',
      mips_special_scoring_flag: ''
    }
  ],
  npi_qp_status: [
    {
      npi: '12345',
      run_date: '2017-02-20T00:00:00.000Z',
      qp_year: '2017',
      qp_reporting_period: '',
      run_number: 1,
      qp_status: ''
    }
  ],
  tin_npi_qp_determination_full: {
    apm_id: '1',
    entity_id: 'AA-AAAAAA',
    run_date: '2017-02-20T00:00:00.000Z',
    qp_year: '2017',
    qp_reporting_period: 'P',
    run_number: 1,
    subdiv: '',
    tin: '123456789',
    npi: '12345',
    qp_status: 'Y',
    apm_patient_count_numerator: 12345,
    apm_patient_count_denominator: 12345,
    apm_patient_threshold_score: 11.11,
    apm_patient_threshold_met: 'Y',
    apm_payment_numerator: 123456789.10,
    apm_payment_denominator: 123456789.10,
    apm_payment_threshold_score: '11.1',
    apm_payment_threshold_met: 'Y'
  }
};
