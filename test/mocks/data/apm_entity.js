const moment = require('moment');

module.exports = () => ({
  id: '0000-0000-0000-0000',
  apm_id: '0000-0000-0000-0000',
  subdiv_id: '0000-0000-0000-0000',
  name: 'Semantic Health',
  start_date: moment.utc()
    .add(-1, 'month')
    .startOf('month')
    .startOf('day')
    .format('YYYY-MM-DD'),
  end_date: moment.utc()
    .add(1, 'month')
    .startOf('month')
    .startOf('day')
    .format('YYYY-MM-DD'),
  dba: 'dba',
  tin: 'XX-XXXXXXX',
  npi: 'XXXXXXXXXX',
  additional_information: '',
  type: '',
  address: {
    zip4: '',
    zip5: '20171',
    state: 'VA',
    city: 'Herndon',
    address_2: 'Suite 420',
    address_1: '13921 Park Center Road'
  }
});
