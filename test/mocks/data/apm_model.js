const moment = require('moment');

module.exports = () => ({
  id: '0000-0000-0000-0000',
  name: 'Semantic Health',
  short_name: 'SH',
  start_date: moment.utc()
    .add(-1, 'month')
    .startOf('month')
    .startOf('day')
    .format('YYYY-MM-DD'),
  end_date: moment.utc()
    .add(1, 'month')
    .startOf('month')
    .startOf('day')
    .format('YYYY-MM-DD'),
  advanced_apm_flag: 'Y',
  mips_special_scoring_flag: 'P',
  apm_quality_reporting_category: 1
});
