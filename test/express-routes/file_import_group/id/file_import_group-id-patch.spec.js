const sinon = require('sinon');
const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();
const environment = require('../../../environment');
const route = require('../../../../app/express-routes/api/v1/file_import_group/id/patch');
const SQSMessageApi = require('../../../../app/services/createSQSJobMessage').SQSMessageApi;

const expect = chai.expect;

describe('Express - /api/v1/file_import_group/:id/patch.js', () => {
  const requireAPIPatch = (overrideMocks = {}) =>
  proxyquire('../../../../app/express-routes/api/v1/file_import_group/id/patch.js',
    Object.assign({
      '../../../../../services/forwardDataWorker': () => {}
    }, overrideMocks));

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname,
        ['../../../../app/express-routes/api/v1/file_import_group/id/patch.js',
          '../../../../app/config']);
      environment.use('dev');
    });

    it('should properly load', () => {
      const route2 = requireAPIPatch();
      expect(route2).to.not.be.undefined;
      expect(route2).to.satisfy(r => r instanceof Function);
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname,
        ['../../../../app/express-routes/api/v1/file_import_group/id/patch.js', '../../../../app/config']);
      environment.use('production');
    });

    it('should properly load', () => {
      const route2 = requireAPIPatch();
      expect(route2).to.not.be.undefined;
      expect(route2).to.satisfy(r => r instanceof Function);
    });
  });

  describe('FileImportGroupUpdateController', () => {
    let controller;
    let createSQSJobMessageStub;

    beforeEach(() => {
      controller = route.FileImportGroupUpdateController;
      createSQSJobMessageStub = sinon.stub(SQSMessageApi, 'create').callsFake(() => Promise.resolve());
    });

    afterEach(() => {
      createSQSJobMessageStub.restore();
    });

    it('should properly load', () => {
      expect(controller).to.not.be.undefined;
      expect(controller.updateStatus).to.not.be.undefined;
    });

    describe('FileImportSourceMap', () => {
      it('should be able to GET', () => {
        const FileImportSourceMap = controller.FileImportSourceMap;
        expect(FileImportSourceMap).to.not.be.undefined;
        expect(FileImportSourceMap['pac-model']).to.equal('pac-model');
      });
    });

    describe('getPublishableFiles()', () => {
      describe('when new files exist', () => {
        it('should return all the files matching the new file\'s type', () => {
          const publishHistory = {
            files: [
              {
                new_file: true,
                import_file_type: 'cmmi-provider'
              },
              {
                new_file: false,
                import_file_type: 'cmmi-provider'
              }
            ]
          };
          const result = controller.getPublishableFiles(publishHistory);
          expect(result.length).to.equal(2);
        });

        it('should set all the files status to publishing', () => {
          const publishHistory = {
            files: [
              {
                new_file: true,
                import_file_type: 'cmmi-provider'
              },
              {
                new_file: false,
                import_file_type: 'cmmi-provider'
              }
            ]
          };
          const result = controller.getPublishableFiles(publishHistory);
          expect(result[0].status).to.equal('publishing');
          expect(result[1].status).to.equal('publishing');
        });

        it('should not return files that do not match the new file\'s type', () => {
          const publishHistory = {
            files: [
              {
                new_file: true,
                import_file_type: 'cmmi-provider'
              },
              {
                new_file: false,
                import_file_type: 'cmmi-provider'
              },
              {
                new_file: false,
                import_file_type: 'cmmi-entity'
              }
            ]
          };
          const result = controller.getPublishableFiles(publishHistory);
          expect(result.length).to.equal(2);
        });
      });

      describe('file type is pac', () => {
        it('should return pac files', () => {
          const publishHistory = {
            files: [
              {
                new_file: true,
                import_file_type: 'pac-provider'
              },
              {
                new_file: false,
                import_file_type: 'pac-model'
              }
            ]
          };
          const result = controller.getPublishableFiles(publishHistory);
          expect(result.length).to.equal(2);
        });

        it('should set the pac files status to publishing', () => {
          const publishHistory = {
            files: [
              {
                new_file: true,
                import_file_type: 'pac-provider'
              },
              {
                new_file: false,
                import_file_type: 'pac-model'
              }
            ]
          };
          const result = controller.getPublishableFiles(publishHistory);
          expect(result[0].status).to.equal('publishing');
          expect(result[1].status).to.equal('publishing');
        });
      });
    });

    describe('queuePublishMessages()', () => {
      it('should create a merge finished message if there are no publishable files', () => {
        const publishableFiles = [];
        const publishHistory = {
          id: '1234567890'
        };
        const fileImportGroup = {
          id: '1234567890'
        };
        const result = controller.queuePublishMessages(publishableFiles, publishHistory, fileImportGroup);
        expect(result.length).to.equal(1);
        expect(createSQSJobMessageStub).to.have.been.called;
        expect(createSQSJobMessageStub.getCall(0).args[1]).to.equal('PUBLISH_HISTORY_MERGE_FINISHED');
      });

      it('should create a merge queue message if there are publishable files', () => {
        const publishableFiles = [
          {
            file_id: '1234567890'
          },
          {
            file_id: '2345678901'
          }
        ];
        const publishHistory = {
          id: '1234567890'
        };
        const fileImportGroup = {
          id: '1234567890'
        };
        controller.queuePublishMessages(publishableFiles, publishHistory, fileImportGroup);
        expect(createSQSJobMessageStub).to.have.been.called;
        expect(createSQSJobMessageStub.getCall(0).args[1]).to.equal('PUBLISH_HISTORY_MERGE_FILE_QUEUE');
      });
    });

    describe('publishFiles()', () => {
      let getPublishHistoriesStub;
      let updatePublishHistoryStub;

      beforeEach(() => {
        getPublishHistoriesStub = sinon.stub(controller, 'getPublishHistories');
        updatePublishHistoryStub = sinon.stub(controller, 'updatePublishHistory');
      });

      afterEach(() => {
        getPublishHistoriesStub.restore();
        updatePublishHistoryStub.restore();
      });

      it('should queue publish messages', async () => {
        getPublishHistoriesStub.callsFake(() => [{
          files: [
            {
              file_id: '1234567890',
              new_file: true,
              import_file_type: 'cmmi-provider'
            },
            {
              file_id: '2345678901',
              new_file: true,
              import_file_type: 'cmmi-provider'
            }
          ]
        }]);

        const fileImportGroup = {
          id: '1234567890'
        };
        await controller.publishFiles(fileImportGroup);
        expect(createSQSJobMessageStub).to.have.been.calledOnce;
        expect(createSQSJobMessageStub.getCall(0).args[1]).to.equal('PUBLISH_HISTORY_MERGE_FILE_QUEUE');
      });

      it('should queue publish messages for all publish histories', async () => {
        getPublishHistoriesStub.callsFake(() => [{
          files: [
            {
              file_id: '1234567890',
              new_file: true,
              import_file_type: 'cmmi-provider'
            },
            {
              file_id: '2345678901',
              new_file: true,
              import_file_type: 'cmmi-provider'
            }
          ]
        },
        {
          files: [
            {
              file_id: '1234567890',
              new_file: true,
              import_file_type: 'pac-provider'
            },
            {
              file_id: '2345678901',
              new_file: true,
              import_file_type: 'pac-provider'
            }
          ]
        }]);
        const fileImportGroup = {
          id: '1234567890'
        };
        await controller.publishFiles(fileImportGroup);
        expect(createSQSJobMessageStub).to.have.calledTwice;
        expect(createSQSJobMessageStub.getCall(0).args[1]).to.equal('PUBLISH_HISTORY_MERGE_FILE_QUEUE');
      });
    });
  });
});
