const Promise = require('bluebird');
const events = require('events');
const stream = require('stream');
const chai = require('chai');
const express = require('express');
const request = require('supertest');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();
const environment = require('../../../../environment');

const expect = chai.expect;

const MockAWS = require('../../../../mocks/services/aws-mock.js');
const MockLogger = require('../../../../mocks/services/logger-mock.js');

const MockRequest = () => () => Promise.try(() => ({
  statusCode: 200,
  body: {
    data: {
      file_import_group: {
        id: 'fake-id',
        status: 'open',
        files: [
          {
            id: '0000-0000',
            status: 'processing'
          }
        ]
      },
      file: {
        id: '0000-0000',
        status: 'processing'
      }
    }
  }
}));

const MockBusboy = () => {
  function Stub() {
    const writeableStream = new stream.Writable();
    const eventEmitter = new events.EventEmitter();
    writeableStream.on = eventEmitter.on;

    const emit = writeableStream.emit.bind(writeableStream);
    writeableStream.emit = () => {};

    setTimeout(() => {
      emit('file', 'file', { pipe: () => {}, on: () => {} }, 'file.png', 'utf8', 'image/png');
    }, 1);

    setTimeout(() => {
      emit('finish', {});
    }, 2);

    return writeableStream;
  }

  return Stub;
};

const MockBusboyNoFileName = () => {
  function Stub() {
    const writeableStream = new stream.Writable();
    const eventEmitter = new events.EventEmitter();
    writeableStream.on = eventEmitter.on;

    const emit = writeableStream.emit.bind(writeableStream);
    writeableStream.emit = () => {};

    setTimeout(() => {
      emit('file', 'file', { pipe: () => {}, on: () => {} }, '', 'utf8', 'image/png');
    }, 1);

    setTimeout(() => {
      emit('finish', {});
    }, 2);

    return writeableStream;
  }

  return Stub;
};

const MockS3UploadStream = () => {
  function Stub() {
    const eventEmitter = new events.EventEmitter();

    setTimeout(() => {
      eventEmitter.emit('uploaded', {});
    }, 100);

    return {
      upload: () => eventEmitter
    };
  }

  return Stub;
};

const MockCreateSQSMessage = () => () => Promise.try(() => {});

describe('Express - /api/v1/file_import_group/:id/upload/post.js', () => {
  const mocks = {};
  const requireUpload = (overrideMocks = {}) =>
   proxyquire('../../../../../app/express-routes/api/v1/file_import_group/id/upload/post.js',
    Object.assign({
      busboy: mocks.busboy,
      's3-upload-stream': mocks.s3UploadStream,
      'request-promise': mocks.request,
      '../../../../../../services/aws': mocks.aws,
      // '../../../../../../services/logger': mocks.logger,
      '../../../../../../services/createSQSMessageForFileUpload': mocks.createSQSMessageForFileUpload
    }, overrideMocks));

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname,
        ['../../../../../app/express-routes/api/v1/file_import_group/id/upload/post.js',
          '../../../../../app/config']);
      environment.use('dev');
      mocks.aws = MockAWS();
      mocks.logger = MockLogger();
      mocks.busboy = MockBusboy();
      mocks.s3UploadStream = MockS3UploadStream();
      mocks.request = MockRequest();
      mocks.createSQSMessageForFileUpload = MockCreateSQSMessage();
    });

    it('should properly load', () => {
      const route = requireUpload();
      expect(route).to.not.be.undefined;
      expect(route).to.satisfy(r => r instanceof Function);
    });

    it('should accept a file upload', () => {
      mocks.busboy = MockBusboyNoFileName();
      const route = requireUpload();

      const app = express();
      app.use(route);

      return request(app)
        .post('/?import-file-type=pac-model')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.file_ids).to.not.be.undefined;
          expect(body.data.file_ids.length).to.be.equal(1);
          expect(body.data.file_ids[0]).to.be.equal('0000-0000');
        });
    });

    it('should reject a file upload to a published group', () => {
      mocks.request = () => Promise.try(() => ({
        statusCode: 200,
        body: {
          data: {
            file_import_group: {
              id: 'fake-id',
              status: 'published',
              files: [
                {
                  id: '0000-0000',
                  status: 'finished'
                }
              ]
            },
            file: {
              id: '0000-0000',
              status: 'finished'
            }
          }
        }
      }));
      const route = requireUpload();

      const app = express();
      app.use(route);

      return request(app)
        .post('/?import-file-type=pac-model')
        .send()
        .expect(400)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    // it('should reject an invalid import file type', () => {
    //   const route = requireUpload();
    //
    //   const app = express();
    //   app.use(route);
    //
    //   return request(app)
    //       .post('/?import-file-type=does-not-exist')
    //       .send()
    //       .expect(400)
    //       .then((res) => {
    //         const body = res.body;
    //         expect(body).to.not.be.undefined;
    //         expect(body.message).to.not.be.undefined;
    //       });
    // });

    it('should handle an exception during processing', () => {
      mocks.request = () => Promise.reject(new Error('fake error'));
      const route = requireUpload();

      const app = express();
      app.use(route);

      return request(app)
        .post('/?import-file-type=pac-model')
        .send()
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should handle a 404 request to the data worker', () => {
      mocks.request = () => Promise.resolve({
        statusCode: 404,
        body: {
          message: 'Not Found'
        }
      });
      const route = requireUpload();

      const app = express();
      app.use(route);

      return request(app)
        .post('/?import-file-type=pac-model')
        .send()
        .expect(404)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should handle a 500 request to the data worker', () => {
      mocks.request = () => Promise.resolve({
        statusCode: 500,
        body: {
          message: 'error'
        }
      });
      const route = requireUpload();

      const app = express();
      app.use(route);

      return request(app)
        .post('/?import-file-type=pac-model')
        .send()
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should handle an exception', () => {
      mocks.createSQSMessageForFileUpload = () => Promise.reject(new Error('error'));
      const route = requireUpload();

      const app = express();
      app.use(route);

      return request(app)
        .post('/?import-file-type=pac-model')
        .send()
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should handle bad requests to data worker - 1', () => {
      const requestMock = sinon.stub(mocks, 'request');

      requestMock.onCall(0).callsFake(() => Promise.resolve({
        statusCode: 200,
        body: {
          data: {
            file_import_group: {
              id: 'fake-id',
              status: 'open',
              files: [
                {
                  id: '0000-0000',
                  status: 'processing'
                }
              ]
            },
            file: {
              id: '0000-0000',
              status: 'processing'
            }
          }
        }
      }));
      requestMock.onCall(1).callsFake(() => Promise.resolve({
        statusCode: 500,
        body: {
          message: 'ERROR'
        }
      }));
      const route = requireUpload();

      const app = express();
      app.use(route);

      return request(app)
        .post('/?import-file-type=pac-model')
        .send()
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should handle bad requests to data worker - 2', () => {
      const requestMock = sinon.stub(mocks, 'request');

      requestMock.onCall(0).callsFake(() => Promise.resolve({
        statusCode: 200,
        body: {
          data: {
            file_import_group: {
              id: 'fake-id',
              status: 'open',
              files: [
                {
                  id: '0000-0000',
                  status: 'processing'
                }
              ]
            },
            file: {
              id: '0000-0000',
              status: 'processing'
            }
          }
        }
      }));
      requestMock.onCall(1).callsFake(() => Promise.resolve({
        statusCode: 404,
        body: {
          message: 'ERROR'
        }
      }));
      const route = requireUpload();

      const app = express();
      app.use(route);

      return request(app)
        .post('/?import-file-type=pac-model')
        .send()
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should handle bad requests to data worker - 3', () => {
      const requestMock = sinon.stub(mocks, 'request');

      requestMock.onCall(0).callsFake(() => Promise.resolve({
        statusCode: 200,
        body: {
          data: {
            file_import_group: {
              id: 'fake-id',
              status: 'open',
              files: [
                {
                  id: '0000-0000',
                  status: 'processing'
                }
              ]
            },
            file: {
              id: '0000-0000',
              status: 'processing'
            }
          }
        }
      }));

      requestMock.onCall(1).callsFake(() => Promise.resolve({
        statusCode: 200,
        body: {
          data: {
            file_import_group: {
              id: 'fake-id',
              status: 'open',
              files: [
                {
                  id: '0000-0000',
                  status: 'processing'
                }
              ]
            },
            file: {
              id: '0000-0000',
              status: 'processing'
            }
          }
        }
      }));

      requestMock.onCall(2).callsFake(() => Promise.resolve({
        statusCode: 500,
        body: {
          message: 'ERROR'
        }
      }));
      const route = requireUpload();

      const app = express();
      app.use(route);

      return request(app)
        .post('/?import-file-type=pac-model')
        .send()
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should handle bad requests to data worker - 4', () => {
      const requestMock = sinon.stub(mocks, 'request');

      requestMock.onCall(0).callsFake(() => Promise.resolve({
        statusCode: 200,
        body: {
          data: {
            file_import_group: {
              id: 'fake-id',
              status: 'open',
              files: [
                {
                  id: '0000-0000',
                  status: 'processing'
                }
              ]
            },
            file: {
              id: '0000-0000',
              status: 'processing'
            }
          }
        }
      }));

      requestMock.onCall(1).callsFake(() => Promise.resolve({
        statusCode: 200,
        body: {
          data: {
            file_import_group: {
              id: 'fake-id',
              status: 'open',
              files: [
                {
                  id: '0000-0000',
                  status: 'processing'
                }
              ]
            },
            file: {
              id: '0000-0000',
              status: 'processing'
            }
          }
        }
      }));

      requestMock.onCall(2).callsFake(() => Promise.resolve({
        statusCode: 404,
        body: {
          message: 'ERROR'
        }
      }));
      const route = requireUpload();

      const app = express();
      app.use(route);

      return request(app)
        .post('/?import-file-type=pac-model')
        .send()
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../../../../app/express-routes/api/v1/file_import_group/id/upload/post.js',
        '../../../../../app/config']);
      environment.use('production');
      mocks.aws = MockAWS();
      mocks.logger = MockLogger();
      mocks.busboy = MockBusboy();
      mocks.s3UploadStream = MockS3UploadStream();
      mocks.request = MockRequest();
      mocks.createSQSMessageForFileUpload = MockCreateSQSMessage();
    });

    it('should properly load', () => {
      const route = requireUpload();
      expect(route).to.not.be.undefined;
      expect(route).to.satisfy(r => r instanceof Function);
    });

    it('should accept a file upload', () => {
      const route = requireUpload();

      const app = express();
      app.use(route);

      return request(app)
        .post('/?import-file-type=pac-model')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.file_ids).to.not.be.undefined;
          expect(body.data.file_ids.length).to.be.equal(1);
          expect(body.data.file_ids[0]).to.be.equal('0000-0000');
        });
    });

    // it('should reject an invalid import file type', () => {
    //   const route = requireUpload();
    //
    //   const app = express();
    //   app.use(route);
    //
    //   return request(app)
    //       .post('/?import-file-type=does-not-exist')
    //       .send()
    //       .expect(400)
    //       .then((res) => {
    //         const body = res.body;
    //         expect(body).to.not.be.undefined;
    //         expect(body.message).to.not.be.undefined;
    //       });
    // });

    it('should handle an exception during processing', () => {
      mocks.request = () => Promise.reject(new Error('fake error'));
      const route = requireUpload();

      const app = express();
      app.use(route);

      return request(app)
        .post('/?import-file-type=pac-model')
        .send()
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should handle a 404 request to the data worker', () => {
      mocks.request = () => Promise.resolve({
        statusCode: 404
      });
      const route = requireUpload();

      const app = express();
      app.use(route);

      return request(app)
        .post('/?import-file-type=pac-model')
        .send()
        .expect(404)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should handle a 500 request to the data worker', () => {
      mocks.request = () => Promise.resolve({
        statusCode: 500
      });
      const route = requireUpload();

      const app = express();
      app.use(route);

      return request(app)
        .post('/?import-file-type=pac-model')
        .send()
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });
  });
});
