const jwt = require('jsonwebtoken');
const Promise = require('bluebird');
const express = require('express');
const request = require('supertest');
const bodyParser = require('body-parser');
const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();
const environment = require('../../environment');

const expect = chai.expect;

const LoggerMock = require('../../mocks/services/logger-mock');

const EuaLdapMock = result => ({
  checkUserPassword: () => Promise.resolve(result == null ? true : result)
});

const SessionMock = () => ({
  createSession: () => Promise.resolve({
    _id: '59f345366d040b479d74ea32',
    username: 'gyqe',
    __v: 0,
    mail: 'gyqe@fake.com',
    name: 'User GYQE',
    permissionGroups: [],
    token: jwt.sign({}, 'secret'),
    session: {
      success: true,
      expiresInSeconds: 895,
      isActive: true,
      username: 'gyqe'
    }
  }),
  deleteSession: () => Promise.resolve(true),
  checkAndExtendSession: () => Promise.resolve(Promise.resolve({
    _id: '59f345366d040b479d74ea32',
    username: 'gyqe',
    __v: 0,
    mail: 'gyqe@fake.com',
    name: 'User GYQE',
    permissionGroups: [],
    token: jwt.sign({}, 'secret'),
    session: {
      success: true,
      expiresInSeconds: 895,
      isActive: true,
      username: 'gyqe'
    }
  }))
});

const UserMock = result => ({
  upsertUser: () => Promise.resolve(result == null ? {} : result)
});

describe('Express - /api/v1/user/login/post.js', () => {
  const mocks = {};
  const requireAPIGet = (overrideMocks = {}) => proxyquire('../../../app/express-routes/api/v1/user/login/post.js',
    Object.assign({
      '../../../../../services/logger': mocks.logger,
      '../../../../../services/euaLdap': mocks.euaLdap,
      '../../../../../services/SessionService': mocks.session,
      '../../../../../services/user': mocks.user
    }, overrideMocks));

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../../app/express-routes/api/v1/user/login/post.js']);
      environment.use('dev');
      mocks.logger = LoggerMock();
      mocks.euaLdap = EuaLdapMock();
      mocks.session = SessionMock();
      mocks.user = UserMock();
    });

    it('should properly load', () => {
      const route = requireAPIGet();
      expect(route).to.not.be.undefined;
      expect(route).to.satisfy(r => r instanceof Function);
    });

    it('should successfully log user in', () => {
      const route = requireAPIGet();

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(route);

      return request(app)
        .post('/')
        .send({
          username: 'abc',
          password: '123'
        })
        .expect(200)
        .then((res) => {
          const data = res.body;
          expect(data.data.user).to.not.be.undefined;
          expect(data.data.user.token).to.not.be.undefined;
        });
    });

    it('should fail to log user in without password', () => {
      const route = requireAPIGet();

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(route);

      return request(app)
        .post('/')
        .send({
          username: 'abc'
        })
        .expect(400)
        .then((res) => {
          const data = res.body;
          expect(data.message).to.not.be.undefined;
        });
    });

    it('should fail to log user in with invalid password', () => {
      mocks.euaLdap = EuaLdapMock(false);
      const route = requireAPIGet();

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(route);

      return request(app)
        .post('/')
        .send({
          username: 'abc',
          password: '123'
        })
        .expect(403)
        .then((res) => {
          const data = res.body;
          expect(data.message).to.not.be.undefined;
        });
    });

    it('should fail to log user in with when error occurs', () => {
      mocks.session = {
        createSession: Promise.reject(new Error('fake'))
      };
      const route = requireAPIGet();

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(route);

      return request(app)
        .post('/')
        .send({
          username: 'abc',
          password: '123'
        })
        .expect(500)
        .then((res) => {
          const data = res.body;
          expect(data.message).to.not.be.undefined;
        });
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../../app/express-routes/api/v1/user/login/post.js']);
      environment.use('production');
      mocks.logger = LoggerMock();
      mocks.euaLdap = EuaLdapMock();
      mocks.session = SessionMock();
      mocks.user = UserMock();
    });

    it('should properly load', () => {
      const route = requireAPIGet();
      expect(route).to.not.be.undefined;
      expect(route).to.satisfy(r => r instanceof Function);
    });

    it('should successfully log user in', () => {
      const route = requireAPIGet();

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(route);

      return request(app)
        .post('/')
        .send({
          username: 'abc',
          password: '123'
        })
        .expect(200)
        .then((res) => {
          const data = res.body;
          expect(data.data.user).to.not.be.undefined;
          expect(data.data.user.token).to.not.be.undefined;
        });
    });

    it('should fail to log user in without password', () => {
      const route = requireAPIGet();

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(route);

      return request(app)
        .post('/')
        .send({
          username: 'abc'
        })
        .expect(400)
        .then((res) => {
          const data = res.body;
          expect(data.message).to.not.be.undefined;
        });
    });

    it('should fail to log user in with invalid password', () => {
      mocks.euaLdap = EuaLdapMock(false);
      const route = requireAPIGet();

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(route);

      return request(app)
        .post('/')
        .send({
          username: 'abc',
          password: '123'
        })
        .expect(403)
        .then((res) => {
          const data = res.body;
          expect(data.message).to.not.be.undefined;
        });
    });

    it('should fail to log user in with when error occurs', () => {
      mocks.session = {
        createSession: Promise.reject(new Error('fake'))
      };
      const route = requireAPIGet();

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(route);

      return request(app)
        .post('/')
        .send({
          username: 'abc',
          password: '123'
        })
        .expect(500)
        .then((res) => {
          const data = res.body;
          expect(data.message).to.not.be.undefined;
        });
    });
  });
});
