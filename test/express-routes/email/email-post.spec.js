const Promise = require('bluebird');
const chai = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();
const express = require('express');
const request = require('supertest');
const bodyParser = require('body-parser');
const StatusCodeError = require('request-promise/errors').StatusCodeError;
const environment = require('../../environment');

const expect = chai.expect;
const MockLogger = require('../../mocks/services/logger-mock');

describe('Express - /api/v1/email/post.js', () => {
  const mocks = {};
  const requireAPIPost = (overrideMocks = {}) => proxyquire('../../../app/express-routes/api/v1/email/post.js',
    Object.assign({
      '../../../../services/logger': mocks.logger,
      '../../../../services/email': mocks.email
    }, overrideMocks));

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../../app/express-routes/api/v1/email/post.js', '../../../app/config']);
      environment.use('dev');
      mocks.email = () => Promise.try(() => ({ }));
      mocks.logger = MockLogger();
    });

    it('should properly load', () => {
      const route = requireAPIPost();
      expect(route).to.not.be.undefined;
      expect(route).to.satisfy(r => r instanceof Function);
    });

    it('should properly handle a request', () => {
      const route = requireAPIPost();
      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(route);
      const reqBody = {
        body: {
          test: 'test'
        }
      };

      return request(app)
        .post('/')
        .send(reqBody)
        .expect(200);
    });

    it('should properly handle an error', () => {
      mocks.email = () => Promise.try(() => {
        throw new StatusCodeError(500, '', '', '');
      });
      const route = requireAPIPost();
      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(route);
      const reqBody = {
        body: {
          test: 'test'
        }
      };

      return request(app)
        .post('/')
        .send(reqBody)
        .expect(500)
        .catch((errResponse) => {
          expect(errResponse).to.not.be.undefined;
        });
    });

    it('should log an error when sending an email throws an error', () => {
      const loggerSpy = sinon.spy(mocks.logger, 'error');
      mocks.email = () => Promise.try(() => {
        throw new StatusCodeError(500, '', '', '');
      });
      const route = requireAPIPost();
      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(route);
      const reqBody = {
        body: {
          test: 'test'
        }
      };

      return request(app)
        .post('/')
        .send(reqBody)
        .expect(500)
        .catch(() => {
          expect(loggerSpy).to.have.been.called;
        });
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../../app/express-routes/api/v1/email/post.js', '../../../app/config']);
      environment.use('production');
      mocks.email = () => Promise.try(() => ({ }));
      mocks.logger = MockLogger();
    });

    it('should properly load', () => {
      const route = requireAPIPost();
      expect(route).to.not.be.undefined;
      expect(route).to.satisfy(r => r instanceof Function);
    });

    it('should properly handle a request', () => {
      const route = requireAPIPost();
      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(route);
      const reqBody = {
        body: {
          test: 'test'
        }
      };

      return request(app)
        .post('/')
        .send(reqBody)
        .expect(200);
    });

    it('should properly handle an error', () => {
      mocks.email = () => Promise.try(() => {
        throw new StatusCodeError(500, '', '', '');
      });
      const route = requireAPIPost();
      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(route);
      const reqBody = {
        body: {
          test: 'test'
        }
      };

      return request(app)
        .options('/')
        .send(reqBody)
        .expect(500)
        .catch((errResponse) => {
          expect(errResponse).to.not.be.undefined;
        });
    });

    it('should log an error when sending an email throws an error', () => {
      const loggerSpy = sinon.spy(mocks.logger, 'error');
      mocks.email = () => Promise.try(() => {
        throw new StatusCodeError(500, '', '', '');
      });
      const route = requireAPIPost();
      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(route);
      const reqBody = {
        body: {
          test: 'test'
        }
      };

      return request(app)
        .post('/')
        .send(reqBody)
        .expect(500)
        .catch(() => {
          expect(loggerSpy).to.have.been.called;
        });
    });
  });
});
