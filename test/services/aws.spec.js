const chai = require('chai');
const environment = require('../environment');

const expect = chai.expect;

describe('Service: AWS', () => {
  const requireAWS = () => require('../../app/services/aws');

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/services/aws', '../../app/config']);
      environment.use('dev');
    });

    it('should properly load', () => {
      const aws = requireAWS();

      expect(aws).to.not.be.undefined;
      expect(aws.S3).to.not.be.undefined;
      expect(aws.SQS).to.not.be.undefined;
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/services/aws', '../../app/config']);
      environment.use('production');
    });

    it('should properly load', () => {
      const aws = requireAWS();

      expect(aws).to.not.be.undefined;
      expect(aws.S3).to.not.be.undefined;
      expect(aws.SQS).to.not.be.undefined;
    });
  });
});
