const chai = require('chai');
const environment = require('../environment');

const expect = chai.expect;

describe('Service: Logger', () => {
  const requireLogger = () => require('../../app/services/logger');

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/services/logger']);
      environment.use('dev');
    });

    it('should have created a new logger', () => {
      const loggerService = requireLogger();
      expect(loggerService).to.not.be.undefined;
    });

    it('should have loging functions', () => {
      const loggerService = requireLogger();
      expect(loggerService.error).to.not.be.undefined;
      expect(loggerService.warn).to.not.be.undefined;
      expect(loggerService.info).to.not.be.undefined;
      expect(loggerService.verbose).to.not.be.undefined;
      expect(loggerService.debug).to.not.be.undefined;
      expect(loggerService.silly).to.not.be.undefined;
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/services/logger']);
      environment.use('production');
    });

    it('should have created a new logger', () => {
      const loggerService = requireLogger();
      expect(loggerService).to.not.be.undefined;
    });

    it('should have loging functions', () => {
      const loggerService = requireLogger();
      expect(loggerService.error).to.not.be.undefined;
      expect(loggerService.warn).to.not.be.undefined;
      expect(loggerService.info).to.not.be.undefined;
      expect(loggerService.verbose).to.not.be.undefined;
      expect(loggerService.debug).to.not.be.undefined;
      expect(loggerService.silly).to.not.be.undefined;
    });
  });
});
