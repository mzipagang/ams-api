const chai = require('chai');
const sinon = require('sinon');
const Promise = require('bluebird');
const aws = require('../../app/services/aws');
const createSQSJobMessage = require('../../app/services/createSQSJobMessage');

const expect = chai.expect;

describe('Service: createSQSJobMessage', () => {
  beforeEach(() => {
    sinon.stub(aws.SQS, 'sendMessage')
      .callsFake((params, cb) => {
        setTimeout(() => cb(null, 1), 1);
      });
  });

  afterEach(() => {
    aws.SQS.sendMessage.restore();
  });

  it('should properly create a message', () => createSQSJobMessage(1, 'NAME', { data: 1 })
    .then((result) => {
      expect(result).to.equal(1);
    }));

  it('should fail because of an invalid priority', () => Promise.try(() => createSQSJobMessage(-1, 'NAME', { data: 1 }))
    .then(() => {
      throw new Error('Should not have been called');
    })
    .catch((err) => {
      expect(err.message).to.equal('Priority can only be values 1 to 2.');
    }));
});
