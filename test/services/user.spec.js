const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();
const environment = require('../environment');

const expect = chai.expect;

const MockDataWorker = () => require('../mocks/services/dataWorker-mock');

describe('Service: user', () => {
  const mocks = {};

  const requireUser = (overrideMocks = {}) => proxyquire('../../app/services/user', Object.assign({
    '../dataWorker': mocks.dataWorker
  }, overrideMocks));

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/services/user', '../../app/config']);
      environment.use('dev');
      mocks.dataWorker = MockDataWorker();
    });

    it('should properly load', () => {
      const user = requireUser();
      expect(user).to.not.be.undefined;
    });

    it('should properly call upsertUser', (done) => {
      const user = requireUser();
      user.upsertUser('test')
        .then(() => {
          done();
        });
    });

    it('should properly call getUser', (done) => {
      const user = requireUser();
      user.getUser('fakeToken')
        .then(() => {
          done();
        });
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/services/user', '../../app/config']);
      environment.use('production');
      mocks.dataWorker = MockDataWorker();
    });

    it('should properly load', () => {
      const user = requireUser();
      expect(user).to.not.be.undefined;
    });

    it('should properly call upsertUser', (done) => {
      const user = requireUser();
      user.upsertUser({ username: 'test', mail: 'test@test.com' })
        .then(() => {
          done();
        });
    });

    it('should properly call getUser', (done) => {
      const user = requireUser();
      user.getUser('123abd654jkl123abd654jkl')
        .then(() => {
          done();
        });
    });
  });
});
