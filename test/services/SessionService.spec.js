const sinon = require('sinon');
const querystring = require('qs');
const { expect } = require('chai');
const SessionService = require('../../app/services/SessionService');
const DataWorker = require('../../app/services/dataWorker');

describe('Service: SessionService', () => {
  const TOKEN = 'TOKEN GUID';
  const QUERY_PARAMS = 'QUERY_PARAMS';
  const EXPECTED_URL = '/api/v1/session?QUERY_PARAMS';
  const USER_RESPONSE = {
    body: {
      data: {
        user: 'Expected User'
      }
    }
  };

  const SUCCESS_RESPONSE = {
    body: {
      data: {
        success: true
      }
    }
  };

  beforeEach(() => {
    sinon.stub(querystring, 'stringify');
    sinon.stub(DataWorker, 'post');
    sinon.stub(DataWorker, 'delete');
    sinon.stub(DataWorker, 'get');

    querystring.stringify.returns(QUERY_PARAMS);
  });

  afterEach(() => {
    querystring.stringify.restore();
    DataWorker.post.restore();
    DataWorker.delete.restore();
    DataWorker.get.restore();
  });

  describe('#createSession', () => {
    const USERNAME = 'User 1';
    const EMAIL = 'email@b.c';
    const FIRST_NAME = 'First name';
    const LAST_NAME = 'Last name';
    const ROLES = ['role1', 'role2'];
    const PERMISSIONS = ['role1', 'role2'];

    let expectedRequest;

    beforeEach(() => {
      expectedRequest = {
        username: USERNAME,
        email: EMAIL,
        firstName: FIRST_NAME,
        lastName: LAST_NAME,
        roles: ROLES,
        permissions: PERMISSIONS,
        type: 'session'
      };

      DataWorker.post.resolves(USER_RESPONSE);
    });

    it('should use the dataworker to create a session', async () => {
      const result = await SessionService.createSession(expectedRequest);
      expect(result).to.equal('Expected User');
      expect(DataWorker.post).to.have.been.calledWith('/api/v1/session', expectedRequest);
    });
  });

  describe('#deleteSession', () => {
    beforeEach(() => {
      DataWorker.delete.returns(SUCCESS_RESPONSE);
    });

    it('should use the dataworker to delete the session', async () => {
      const result = await SessionService.deleteSession(TOKEN);
      expect(result).to.be.true;
      expect(querystring.stringify).to.have.been.calledWith({ token: TOKEN });
      expect(DataWorker.delete).to.have.been.calledWith(EXPECTED_URL);
    });
  });

  describe('#checkAndExtendSession', () => {
    beforeEach(() => {
      DataWorker.get.returns(USER_RESPONSE);
    });

    it('should use the dataworker to extend the session', async () => {
      const result = await SessionService.checkAndExtendSession(TOKEN);
      expect(result).to.equal('Expected User');
      expect(querystring.stringify).to.have.been.calledWith({ token: TOKEN, extend: true });
      expect(DataWorker.get).to.have.been.calledWith(EXPECTED_URL);
    });
  });

  describe('#checkSession', () => {
    beforeEach(() => {
      DataWorker.get.returns(USER_RESPONSE);
    });

    it('should use the dataworker to check the session', async () => {
      const result = await SessionService.checkSession(TOKEN);
      expect(result).to.equal('Expected User');
      expect(querystring.stringify).to.have.been.calledWith({ token: TOKEN });
      expect(DataWorker.get).to.have.been.calledWith(EXPECTED_URL);
    });
  });
});
