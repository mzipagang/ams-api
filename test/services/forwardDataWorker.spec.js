const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
const Promise = require('bluebird');
const environment = require('../environment');

const expect = chai.expect;

const DataWorker = () => require('../mocks/services/dataWorker-mock');
const LoggerMock = require('../mocks/services/logger-mock');

describe('Service: ForwardDataWorker', () => {
  const mocks = {};

  const requireForwardDataWorker = (overrideMocks = {}) =>
  proxyquire('../../app/services/forwardDataWorker', Object.assign({
    '../dataWorker': mocks.dataWorker,
    '../logger': mocks.logger
  }, overrideMocks));

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/services/forwardDataWorker', '../../app/config']);
      environment.use('dev');
      mocks.dataWorker = DataWorker();
      mocks.logger = LoggerMock();
    });

    it('should properly load', () => {
      const forwardDataWorker = requireForwardDataWorker();

      expect(forwardDataWorker).to.not.be.undefined;
    });

    it('should successfully process a request', (done) => {
      const forwardDataWorker = requireForwardDataWorker();
      const req = {
        method: 'get',
        originalUrl: 'testurl',
        body: {},
        headers: {
          'x-tin': '123456789'
        }
      };

      const data = {
        status: () => data,
        json: () => done()
      };
      forwardDataWorker(req, data, () => {});
    });

    it('should handle an error in request', (done) => {
      const loggerInfo = sinon.spy(mocks.logger, 'error');
      const forwardDataWorker = requireForwardDataWorker();

      const req = {
        method: 'get',
        originalUrl: 'testurl',
        body: {},
        headers: {}
      };

      const data = {
        status: () => data,
        json: () => { throw new Error('error in request'); }
      };
      forwardDataWorker(req, data, () => {
        expect(loggerInfo).to.have.been.called;
        done();
      });
    });

    it('should call data worker', () => {
      const dataWorkerSpy = sinon.spy(mocks, 'dataWorker');
      const forwardDataWorker = requireForwardDataWorker();

      const req = {
        method: 'get',
        originalUrl: 'testurl',
        body: {},
        headers: {}
      };

      const data = {
        status: () => data,
        json: () => data
      };

      return Promise.try(() => {
        forwardDataWorker(req, data, () => {});
      })
        .then(() => {
          expect(dataWorkerSpy).to.have.been.called;
        });
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/services/forwardDataWorker', '../../app/config']);
      environment.use('production');
      mocks.dataWorker = DataWorker();
      mocks.logger = LoggerMock();
    });

    it('should properly load', () => {
      const forwardDataWorker = requireForwardDataWorker();

      expect(forwardDataWorker).to.not.be.undefined;
    });

    it('should successfully process a request', (done) => {
      const forwardDataWorker = requireForwardDataWorker();
      const req = {
        method: 'get',
        originalUrl: 'testurl',
        body: {},
        headers: {}
      };

      const data = {
        status: () => data,
        json: () => done()
      };
      forwardDataWorker(req, data, () => {});
    });

    it('should handle an error in request', (done) => {
      const loggerInfo = sinon.spy(mocks.logger, 'error');
      const forwardDataWorker = requireForwardDataWorker();

      const req = {
        method: 'get',
        originalUrl: 'testurl',
        body: {},
        headers: {}
      };

      const data = {
        status: () => data,
        json: () => { throw new Error('error in request'); }
      };
      forwardDataWorker(req, data, () => {
        expect(loggerInfo).to.have.been.called;
        done();
      });
    });

    it('should call data worker', () => {
      const dataWorkerSpy = sinon.spy(mocks, 'dataWorker');
      const forwardDataWorker = requireForwardDataWorker();

      const req = {
        method: 'get',
        originalUrl: 'testurl',
        body: {},
        headers: {}
      };

      const data = {
        status: () => data,
        json: () => data
      };

      return Promise.try(() => {
        forwardDataWorker(req, data, () => {});
      })
        .then(() => {
          expect(dataWorkerSpy).to.have.been.called;
        });
    });
  });
});
