const proxyquire = require('proxyquire').noCallThru();
const chai = require('chai');
const sinon = require('sinon');
const environment = require('../environment');
const config = require('../../app/config');

const expect = chai.expect;

const MockLogger = require('../mocks/services/logger-mock');
const MockNodemailer = require('../mocks/services/nodemailer-mock');

describe('Services: Email', () => {
  const mocks = {};

  const requireEmail = (overrideMocks = {}) => proxyquire('../../app/services/email', Object.assign({
    '../logger': mocks.logger,
    nodemailer: mocks.nodemailer,
    '../../app/config': mocks.config
  }, overrideMocks));

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/services/email', '../../app/config']);
      environment.use('dev');
      mocks.logger = MockLogger();
      mocks.nodemailer = MockNodemailer();
      mocks.config = require('../../app/config');
    });

    it('should load module', () => {
      const emailService = requireEmail();

      expect(emailService).to.not.be.undefined;
      expect(emailService).to.be.a('function');
    });

    it('should set auth parameters if username and password provided', () => {
      mocks.config.EMAIL_API.PASSWORD = 'SemanticBits';
      mocks.config.EMAIL_API.USERNAME = 'SemanticBits';
      const nodeMailerSpy = sinon.spy(mocks.nodemailer, 'createTransport');
      const emailService = requireEmail();

      return emailService('', '')
        .then(() => {
          expect(nodeMailerSpy).to.have.been.calledWith({
            auth: {
              accessToken: mocks.config.EMAIL_API.PASSWORD,
              user: mocks.config.EMAIL_API.USERNAME
            },
            host: mocks.config.EMAIL_API.URI,
            port: mocks.config.EMAIL_API.PORT,
            secure: mocks.config.EMAIL_API.IS_SECURE
          });
          mocks.nodemailer.createTransport.restore();
        });
    });

    it('should properly handle a request', () => {
      const emailService = requireEmail();

      return emailService('testfile.txt has been uploaded', 'testfile.txt has been uploaded')
        .then((resp) => {
          expect(resp).to.not.be.undefined;
          expect(resp.options).to.not.be.undefined;
          expect(resp.config.host).to.equal(config.EMAIL_API.URI);
          expect(resp.config.port).to.equal(config.EMAIL_API.PORT);
          expect(resp.config.secure).to.equal(config.EMAIL_API.IS_SECURE);
          expect(resp.config.auth).to.be.undefined;
          expect(JSON.stringify(resp.options)).to.equal(JSON.stringify({
            from: 'fakePerson@semanticbits.com',
            to: 'fakePerson@semanticbits.com',
            subject: 'testfile.txt has been uploaded',
            text: 'testfile.txt has been uploaded'
          }));
        });
    });

    it('should send a proper error response when there is an error', () => {
      const transport = {
        sendMail: () => {
          throw new Error('fake error');
        }
      };
      sinon.stub(mocks.nodemailer, 'createTransport').callsFake(() => transport);
      const emailService = requireEmail();

      return emailService('', '')
        .catch((err) => {
          expect(err).to.not.be.undefined;
        });
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/services/email', '../../app/config']);
      environment.use('production');
      mocks.logger = MockLogger();
      mocks.nodemailer = MockNodemailer();
      mocks.config = require('../../app/config');
    });

    it('should load module', () => {
      const emailService = requireEmail();

      expect(emailService).to.not.be.undefined;
      expect(emailService).to.be.a('function');
    });

    it('should set auth parameters if username and password provided', () => {
      mocks.config.EMAIL_API.PASSWORD = 'SemanticBits';
      mocks.config.EMAIL_API.USERNAME = 'SemanticBits';
      const nodeMailerSpy = sinon.spy(mocks.nodemailer, 'createTransport');
      const emailService = requireEmail();

      return emailService('', '')
        .then(() => {
          expect(nodeMailerSpy).to.have.been.calledWith({
            auth: {
              accessToken: mocks.config.EMAIL_API.PASSWORD,
              user: mocks.config.EMAIL_API.USERNAME
            },
            host: mocks.config.EMAIL_API.URI,
            port: mocks.config.EMAIL_API.PORT,
            secure: mocks.config.EMAIL_API.IS_SECURE
          });
          mocks.nodemailer.createTransport.restore();
        });
    });

    it('should properly handle a request', () => {
      const emailService = requireEmail();

      return emailService('testfile.txt has been uploaded', 'testfile.txt has been uploaded')
        .then((resp) => {
          expect(resp).to.not.be.undefined;
        });
    });

    it('should send a proper error response when there is an error', () => {
      const transport = {
        sendMail: () => {
          throw new Error('fake error');
        }
      };
      sinon.stub(mocks.nodemailer, 'createTransport').callsFake(() => transport);
      const emailService = requireEmail();

      return emailService('', '')
        .catch((err) => {
          expect(err).to.not.be.undefined;
        });
    });
  });
});
