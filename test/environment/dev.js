module.exports = {
  use: () => {
    delete process.env.AWS_ACCESS_KEY_ID;
    delete process.env.AWS_SECRET_ACCESS_KEY;
    process.env.PORT = 12345;
    process.env.NODE_ENV = 'dev';
    process.env.ALLOWED_ORIGINS = 'http://localhost:4000,http://localhost:4200,http://localhost:5000,http://localhost';
    process.env.SQS_QUEUE_URL = 'http://0.0.0.0:12345/fake-queue';
  }
};
