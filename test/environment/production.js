module.exports = {
  use: () => {
    process.env.PORT = 54321;
    process.env.AWS_ACCESS_KEY_ID = 'somekey';
    process.env.AWS_SECRET_ACCESS_KEY = 'somesecretkey';
    process.env.NODE_ENV = 'production';
    process.env.ALLOWED_ORIGINS = 'http://api.ams-dev.semanticbits.com';
    process.env.SQS_QUEUE_URL = 'http://0.0.0.0:12345/fake-queue';
  }
};
