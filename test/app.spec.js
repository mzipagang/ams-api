const Promise = require('bluebird');
const sinon = require('sinon');
const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();
const environment = require('./environment');

const expect = chai.expect;

const ExpressMock = require('./mocks/services/express-mock');
const LoggerMock = require('./mocks/services/logger-mock');
const ExpressFileRouterMock = require('./mocks/services/expressFileRouter-mock');

describe('App', () => {
  const mocks = {};
  const requireApp = () => proxyquire('../app', {
    './services/express': mocks.express,
    './services/logger': mocks.logger,
    './services/expressFileRouter': mocks.expressFileRouter
  });

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../app', '../app/config']);
      environment.use('dev');

      mocks.express = ExpressMock();
      mocks.logger = LoggerMock();
      mocks.expressFileRouter = ExpressFileRouterMock();
    });

    it('should require index.js', () => {
      const loggerInfo = sinon.spy(mocks.logger, 'info');
      const app = requireApp();

      return app.express.then(() => {
        expect(loggerInfo).to.have.been.called;
      });
    });

    it('should handle an error when starting express', () => {
      sinon.stub(mocks.express, 'start').callsFake(() => Promise.try(() => {
        throw new Error('Failed to start server');
      }));

      const loggerError = sinon.spy(mocks.logger, 'error');
      const app = requireApp();

      return app.express.then(() => {
        expect(loggerError).to.have.been.called;
      });
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../app', '../app/config']);
      environment.use('production');

      mocks.express = ExpressMock();
      mocks.logger = LoggerMock();
      mocks.expressFileRouter = ExpressFileRouterMock();
    });

    it('should require index.js', () => {
      const loggerInfo = sinon.spy(mocks.logger, 'info');
      const app = requireApp();

      return app.express.then(() => {
        expect(loggerInfo).to.have.been.called;
      });
    });

    it('should handle an error when starting express', () => {
      sinon.stub(mocks.express, 'start').callsFake(() => Promise.try(() => {
        throw new Error('Failed to start server');
      }));

      const loggerError = sinon.spy(mocks.logger, 'error');
      const app = requireApp();

      return app.express.then(() => {
        expect(loggerError).to.have.been.called;
      });
    });
  });
});
