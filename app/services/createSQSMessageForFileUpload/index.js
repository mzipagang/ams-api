const moment = require('moment');
const Promise = require('bluebird');
const config = require('../../config');
const AWS = require('../aws');

const SQS = AWS.SQS;
const sendMessage = Promise.promisify(SQS.sendMessage.bind(SQS));

module.exports = (key) => {
  const Message = {
    scanResult: {
      bucket: config.AWS.S3_BUCKET,
      key,
      infected: false
    }
  };
  const Body = {
    Type: 'Notification',
    MessageId: '01234567-0123-0123-0123-012345678901',
    TopicArn: 'arn:aws:sns:us-east-1:012345678901:ams-dev-uploads-topic',
    Subject: 'Amazon S3 Notification',
    Message: JSON.stringify(Message),
    Timestamp: moment.utc().format(),
    SignatureVersion: '1',
    Signature: '...',
    SigningCertURL: 'http://0.0.0.0',
    UnsubscribeURL: 'http://0.0.0.0'
  };
  const params = {
    DelaySeconds: 10,
    MessageBody: JSON.stringify(Body),
    QueueUrl: config.AWS.SQS_QUEUE_URL
  };
  return sendMessage(params);
};
