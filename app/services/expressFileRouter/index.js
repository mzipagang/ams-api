const path = require('path');
const expressFileRouter = require('express-file-router');

module.exports = {
  load: () => expressFileRouter.load(path.join(__dirname, '../../express-routes'))
};
