module.exports = permissionName =>
  function ensurePermission(req, res, next) {
    if (res.locals.isSystemRequest) {
      next();
      return;
    }

    const user = res.locals.user || {};
    const permissionGroups = user.permissionGroups || [];
    const result = permissionGroups.some(permissionGroup =>
      permissionGroup.permissions && permissionGroup.permissions.find(permission => permission === permissionName)
    );

    if (result) {
      next();
    } else {
      res.status(401).json({
        message: 'Your session is not authorized.'
      });
    }
  };
