const querystring = require('qs');
const Promise = require('bluebird');
const DataWorker = require('../dataWorker');

module.exports = {
  upsertUser: user =>
    Promise.try(() => DataWorker.post('/api/v1/user', user))
      .then(response => response.body.data.user),

  getUser: username => Promise.try(() =>
      DataWorker.get(`/api/v1/user/${querystring.stringify({ username })}`))
        .then(response => response.body.data.user)
};
