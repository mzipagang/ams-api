const logger = require('../logger');
const DataWorker = require('../dataWorker');

module.exports = (req, res, next) => {
  const headers = {};
  Object.keys(req.headers).filter(key => key === 'authorization' || key.indexOf('x-') === 0).forEach((key) => {
    headers[key] = req.headers[key];
  });

  return DataWorker(req.method, req.originalUrl, req.body, headers)
      .then((response) => {
        res.status(response.statusCode).json(response.body);
      })
      .catch((err) => {
        logger.error(err);
        next(err);
      });
};
