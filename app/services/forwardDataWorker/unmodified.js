const request = require('request');
const config = require('../../config');

module.exports = (req, res) => {
  request({
    method: req.method,
    uri: `${config.DATA_WORKER.ROOT_URL}${req.originalUrl}`,
    body: req.body,
    json: true,
    simple: false,
    resolveWithFullResponse: true,
    headers: req.headers
  }).pipe(res);
};

