const Promise = require('bluebird');
const config = require('../../config');
const logger = require('../logger');
const ldapPool = require('../ldapConnect')(config.LDAP.URI, config.LDAP.USER.DN, config.LDAP.USER.PW);

const searchForUser = (client, username) => new Promise((resolve, reject) => {
  if (username.startsWith('test-user') && config.TEST.ENABLE_TEST_USERS) {
    const testUser = config.TEST.USERS.find(user => user.username === username &&
      user.ismemberof.includes(`cn=${config.LDAP.JOB_CODE},ou=Groups,dc=cms,dc=hhs,dc=gov`));

    if (testUser) {
      const user = Object.assign({}, testUser);
      delete user.username;
      delete user.password;
      resolve(testUser);
      return;
    }

    resolve(null);
    return;
  }

  const usersFound = [];
  const opts = {
    filter: `(&(ismemberof=cn\\=${config.LDAP.JOB_CODE}\\,ou\\=Groups\\,dc\\=cms\\,dc\\=hhs\\,dc\\=gov)` +
      `(uid=${username}))`,
    scope: 'one',
    attributes: ['dn', 'ismemberof', 'mail', 'givenName', 'sn'],
    limit: 1
  };
  client.search('ou=People,dc=cms,dc=hhs,dc=gov', opts)
      .then((res) => {
        res.on('searchEntry', (entry) => {
          usersFound.push(entry.object);
        });
        res.on('error', (err) => {
          reject(err);
        });
        res.on('end', () => {
          resolve(usersFound[0]);
        });
      })
      .catch(reject);
});

const comparePassword = (client, username, password) => {
  if (username.startsWith('test-user') && config.TEST.ENABLE_TEST_USERS) {
    const testUser = config.TEST.USERS.find(user => user.username === username && user.password === password);

    if (testUser) {
      return Promise.resolve(true);
    }

    return Promise.resolve(false);
  }

  return client.compare(`uid=${username},ou=People,dc=cms,dc=hhs,dc=gov`, 'userPassword', password);
};

module.exports = {
  checkUserPassword: (username, password) => {
    let client;
    return Promise.coroutine(function* handler() {
      client = yield ldapPool.acquire();
      const user = yield searchForUser(client, username);

      if (!user) {
        return undefined;
      }

      if (yield comparePassword(client, username, password)) {
        return user;
      }

      return null;
    })()
        .catch((err) => {
          logger.error(err);
          throw err;
        })
        .finally(() => {
          ldapPool.release(client);
        });
  }
};
