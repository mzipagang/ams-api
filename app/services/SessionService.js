const querystring = require('qs');
const DataWorker = require('./dataWorker');

class SessionService {
  static async createSession(sessionUser) {
    const request = {
      username: sessionUser.username,
      email: sessionUser.email,
      firstName: sessionUser.firstName,
      lastName: sessionUser.lastName,
      roles: sessionUser.roles,
      permissions: sessionUser.permissions,
      type: 'session'
    };
    const response = await DataWorker.post('/api/v1/session', request);
    return response.body.data.user;
  }

  static async deleteSession(token) {
    const queryParams = querystring.stringify({ token });
    const response = await DataWorker.delete(`/api/v1/session?${queryParams}`);
    return response.body.data.success;
  }

  static async checkAndExtendSession(token) {
    const queryParams = querystring.stringify({ token, extend: true });
    const response = await DataWorker.get(`/api/v1/session?${queryParams}`);
    return response.body.data.user;
  }

  static async checkSession(token) {
    const queryParams = querystring.stringify({ token });
    const response = await DataWorker.get(`/api/v1/session?${queryParams}`);
    return response.body.data.user;
  }
}

module.exports = SessionService;
