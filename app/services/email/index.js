const nodemailer = require('nodemailer');
const config = require('../../config');
const Promise = require('bluebird');

const transporter = nodemailer.createTransport({
  host: config.EMAIL_API.URI,
  port: config.EMAIL_API.PORT,
  secure: config.EMAIL_API.IS_SECURE,
  auth: (config.EMAIL_API.USERNAME || config.EMAIL_API.PASSWORD) ? {
    user: config.EMAIL_API.USERNAME,
    accessToken: config.EMAIL_API.PASSWORD
  } : undefined
});

const sendMail = Promise.promisify(transporter.sendMail.bind(transporter));

module.exports = (subject, message) => Promise.try(() => {
  const mailOptions = {
    from: config.EMAIL_API.NOTIFICATION_SENDER,
    to: config.EMAIL_API.NOTIFICATION_RECEIVERS,
    subject,
    text: message
  };

  return sendMail(mailOptions);
});
