const Promise = require('bluebird');
const AWS = require('aws-sdk');
const config = require('../../config');

AWS.config.setPromisesDependency(Promise);

AWS.config.update({
  region: 'us-east-1'
});

const setUpS3 = () => {
  const s3Config = {
    region: 'us-east-1',
    s3ForcePathStyle: true,
    accessKeyId: config.AWS.ACCESS_KEY_ID,
    secretAccessKey: config.AWS.SECRET_ACCESS_KEY
  };

  /* istanbul ignore if */
  /* istanbul ignore else */
  if (config.AWS.LOCAL_AWS) {
    s3Config.endpoint = new AWS.Endpoint(config.DEV.S3);
  }

  return new AWS.S3(s3Config);
};

const setupSQS = () => {
  const sqsConfig = {
    region: 'us-east-1',
    apiVersion: '2012-11-05',
    accessKeyId: config.AWS.ACCESS_KEY_ID,
    secretAccessKey: config.AWS.SECRET_ACCESS_KEY
  };

  /* istanbul ignore if */
  /* istanbul ignore else */
  if (config.AWS.LOCAL_AWS) {
    sqsConfig.endpoint = new AWS.Endpoint(config.DEV.SQS);
  }

  return new AWS.SQS(sqsConfig);
};

module.exports = {
  S3: setUpS3(),
  SQS: setupSQS()
};
