const Promise = require('bluebird');
const jwt = require('jsonwebtoken');
const config = require('../../config');
const SessionService = require('../SessionService');

module.exports = (req, res, next) => {
  Promise.coroutine(function* handler() {
    const token = (req.headers.authorization || '').replace('Bearer ', '');
    if (!token) {
      res.status(401).json({
        message: 'No authentication token provided.'
      });
      return;
    }

    const tokenData = jwt.verify(token, config.JWT.SECRET);

    if (tokenData && tokenData.type === 'system') {
      res.locals.isSystemRequest = true;
      next();
      return;
    }

    const user = yield SessionService.checkAndExtendSession(token);

    if (user) {
      res.locals.user = user;
      next();
    } else {
      res.status(401).json({
        message: 'Your session is no longer valid.'
      });
    }
  })()
    .catch(next);
};
