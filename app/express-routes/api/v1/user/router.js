const sessionCheckMiddleware = require('../../../../services/sessionCheckMiddleware');
// const ensurePermission = require('../../../../services/ensurePermission');

module.exports = (router) => {
  router.get('/', sessionCheckMiddleware);

  // In order to check permissions, use the ensurePermission middleware
  // permission check must always be after session check

  // router.get('/', ensurePermission('report'));

  return router;
};
