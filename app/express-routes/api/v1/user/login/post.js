const Promise = require('bluebird');
const logger = require('../../../../../services/logger');
const EUALDAP = require('../../../../../services/euaLdap');
const SessionService = require('../../../../../services/SessionService');
const User = require('../../../../../services/user');
const tcomb = require('tcomb');

/**
 * @swagger
 * /api/v1/user/login:
 *   post:
 *     description: Get session token from username and password
 *     produces:
 *       - application/json
 *     tags:
 *       - User
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Credentials
 *         schema:
 *           type: object
 *           properties:
 *             username:
 *               type: string
 *               description: Username
 *               example: 'abcd'
 *             password:
 *               type: string
 *               description: Password
 *               example: '0123456789'
 *     responses:
 *       200:
 *         description: Returns a session token
 *         schema:
 *           type: object
 *           properties:
 *             data:
 *               properties:
 *                 token:
 *                   type: string
 *                   example: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IkFCQ0QiLCJ0eXBlIjoic2Vzc2lvbiIsImV4cGlyZXNJblNlY29uZHMiOjkwMCwiaWF0IjoxNDk3NjQ0MjUzfQ.NAJtfRjsqXfgHvNzOMbhthGf4y04ixMhOAiwqxtHvt8"
 */

module.exports = (req, res) => {
  Promise.coroutine(function* get() {
    const usernameIn = req.body.username;
    const password = req.body.password;

    if (!tcomb.String.is(usernameIn) || !tcomb.String.is(password)) {
      res.status(400).json({
        message: 'A valid username and password is required.'
      });
      return;
    }

    const username = usernameIn.toLowerCase();

    const euaUser = yield EUALDAP.checkUserPassword(username, password);

    if (!euaUser) {
      res.status(403).json({
        message: 'Sorry, your username and/or password are incorrect.'
      });
      return;
    }

    const upsertedUser = yield User.upsertUser({
      username,
      mail: euaUser.mail,
      firstName: euaUser.givenName,
      lastName: euaUser.sn
    });

    const jwtUser = {
      username: upsertedUser.username,
      email: upsertedUser.mail,
      firstName: upsertedUser.firstName,
      lastName: upsertedUser.lastName,
      roles: upsertedUser.roles,
      permissions: upsertedUser.permissions
    };

    const user = yield SessionService.createSession(jwtUser);

    res.status(200).json({
      data: {
        user
      }
    });
  })()
      .catch((err) => {
        logger.error(err);
        res.status(500).json({ message: 'Unexpected error' });
      });
};
