const logger = require('../../../../services/logger');
const email = require('../../../../services/email');

/**
 * @swagger
 * /api/v1/email:
 *   post:
 *     description: Send a notification email
 *     produces:
 *       - application/json
 *     tags:
 *       - Email
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Email parameters for notification email
 *         schema:
 *           type: object
 *           properties:
 *             fileName:
 *               type: string
 *               description: Name of file being imported
 *               example: 'testFile.txt'
 *             status:
 *               type: string
 *               description: Status of file importing
 *               example: 'file was successfully uploaded'
 *             id:
 *               type: string
 *               description: Internal file id for processing file
 *               example: 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX'
 *     responses:
 *       200:
 *         description: Returns a success message if email was sent
 *         schema:
 *           type: object
 *           properties:
 *             accepted:
 *               type: array
 *               description: recipient addresses that were accepted by the server
 *               example: 'fake@semanticbits.com'
 *               items:
 *                 type: string
 *             rejected:
 *               type: array
 *               description: recipient addresses that were rejected by the server
 *               example: 'fake@semanticbits.com'
 *               items:
 *                 type: string
 *             response:
 *               type: string
 *               description: SMTP response from the server
 *               example: '250 Ok'
 *             envelope:
 *               type: object
 *               description: envelope object for the message
 *             messageId:
 *               type: string
 *               description: message id for sent message
 *               example: '<XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX@semanticbits.com>'
 */

module.exports = (req, res) => {
  const subject = `${req.body.fileName}: ${req.body.status}`;
  const message = `File Processing Id: ${req.body.id}\nFile Name: ${req.body.fileName}\nStatus: ${req.body.status}`;

  email(subject, message)
    .then((response) => {
      res.status(200).json(response);
    })
    .catch((err) => {
      logger.error(err);
      res.status(500).json(err);
    });
};

