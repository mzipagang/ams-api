const Promise = require('bluebird');
const Busboy = require('busboy');
const S3UploadStream = require('s3-upload-stream');
const request = require('request-promise');
const logger = require('../../../../../../services/logger');
const AWS = require('../../../../../../services/aws');
const config = require('../../../../../../config');
const createSQSMessageForFileUpload = require('../../../../../../services/createSQSMessageForFileUpload');

const s3Stream = S3UploadStream(AWS.S3);

const uploadS3 = (readStream, bucket, key, fileType) =>
    new Promise((resolve, reject) => {
      try {
        const upload = s3Stream.upload({
          Bucket: bucket,
          Key: key,
          ContentType: fileType
        });

        upload.on('error', err => reject(err));
        upload.on('uploaded', () => resolve());

        readStream.pipe(upload);
      } catch (err) {
        reject(err);
      }
    });

const getFileImportGroup = Promise.coroutine(function* getFileImportGroup(id, headers) {
  const response = yield request({
    method: 'GET',
    uri: `${config.DATA_WORKER.ROOT_URL}/api/v1/file_import_group/${id}`,
    headers,
    json: true,
    simple: false,
    resolveWithFullResponse: true
  });

  if (response.statusCode === 200) {
    return response.body;
  }

  if (response.statusCode === 404) {
    return null;
  }

  throw new Error(response.body && response.body.message);
});

const startFileImport = Promise.coroutine(function* startFileImport(id, originalFileName, headers) {
  const response = yield request({
    method: 'POST',
    uri: `${config.DATA_WORKER.ROOT_URL}/api/v1/file_import_group/${id}/file`,
    body: {
      file_name: originalFileName,
      status: 'uploading'
    },
    json: true,
    simple: false,
    resolveWithFullResponse: true,
    headers: {
      authorization: headers.authorization
    }
  });

  if (response.statusCode === 200) {
    return response.body;
  }

  if (response.statusCode === 404) {
    return null;
  }

  throw new Error(response.body && response.body.message);
});

const updateFile = Promise.coroutine(function* updateFile(fileImportGroupId, fileId, update, headers) {
  const response = yield request({
    method: 'PATCH',
    uri: `${config.DATA_WORKER.ROOT_URL}/api/v1/file_import_group/${fileImportGroupId}/file/${fileId}`,
    headers,
    body: update,
    json: true,
    simple: false,
    resolveWithFullResponse: true
  });

  if (response.statusCode === 200) {
    return response.body;
  }

  if (response.statusCode === 404) {
    return null;
  }

  throw new Error(response.body && response.body.message);
});

const getFileImportById = Promise.coroutine(function* getFileImportById(id, headers) {
  const response = yield request({
    method: 'GET',
    uri: `${config.DATA_WORKER.ROOT_URL}/api/v1/file_import_group/${id}`,
    headers,
    json: true,
    simple: false,
    resolveWithFullResponse: true
  });

  if (response.statusCode === 200) {
    return response.body;
  }

  if (response.statusCode === 404) {
    return null;
  }

  throw new Error(response.body && response.body.message);
});

/**
 * @swagger
 * /api/v1/file_import_group/{ID}/upload:
 *   post:
 *     description: Upload a file
 *     tags:
 *       - File Import Group
 *     security:
 *       - ApiKeyAuth: []
 *     consumes:
 *       - multipart/form-data
 *     parameters:
 *       - name: file
 *         description: The file to upload
 *         in: formData
 *         required: true
 *         type: file
 *       - name: ID
 *         in: path
 *         description: ID of File Import Group
 *         type: string
 *         required: true
 *       - in: query
 *         name: import-file-type
 *         description: The type of file to upload
 *         type: string
 *         enum:
 *           - pac-model
 *           - pac-entity
 *           - pac-provider
 *           - pac-subdiv
 *           - pac-qp_status
 *           - pac-qp_score
 *           - pac-lvt
 *         required: false
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Returns array of newly created file ids
 *         schema:
 *           type: object
 *           properties:
 *             data:
 *               type: object
 *               properties:
 *                 file_ids:
 *                   type: array
 *                   items:
 *                     type: string
 *                     example: 5a59081f-87aa-48b8-8dba-680b5cbf2ac3
 */

module.exports = (req, res) => {
  Promise.coroutine(function* post() {
    if (!(yield getFileImportById(req.params.id, req.headers))) {
      res.status(404).json({ message: 'File Import Group ID does not exist' });
      return;
    }

    const busboy = new Busboy({ headers: req.headers });
    const fileUploads = [];
    const errors = [];

    busboy.on('file', (fieldname, fileStream, filename, encoding, mimetype) => {
      fileUploads.push(
          Promise.coroutine(function* get() {
            let originalFilename = filename;
            if (originalFilename.length === 0) {
              originalFilename = 'unnamed';
            }
            let safeFilename = filename.replace(/[^a-zA-Z0-9!\-_.*'()]/g, '');
            if (safeFilename.length === 0) {
              safeFilename = 'unnamed';
            }

            const fileImportGroupData = (
              yield startFileImport(req.params.id, originalFilename, req.headers)
            ).data;
            const fileImportGroup = fileImportGroupData.file_import_group;
            const file = fileImportGroupData.file;

            const fileLocation = `${fileImportGroup.id}/${file.id}/${safeFilename}`;
            yield uploadS3(fileStream, config.AWS.S3_BUCKET, fileLocation, mimetype);
            yield updateFile(fileImportGroup.id, file.id, {
              status: 'pending',
              file_location: `${config.AWS.S3_BUCKET}:${fileLocation}`
            }, req.headers);

            if (config.AWS.LOCAL_AWS) {
              yield createSQSMessageForFileUpload(fileLocation);
            }

            return file.id;
          })()
              .catch((err) => {
                logger.error(err);
                errors.push(err);
                fileStream.on('data', () => {}); // Hack to get file to finish
              })
      );
    });

    busboy.on('finish', () => {
      Promise.all(fileUploads)
          .then((fileIds) => {
            if (errors.length > 0) {
              res.status(500).json({ message: 'Error while uploading' });
              return;
            }

            res.json({ data: { file_ids: fileIds } });
          })
          .catch((err) => {
            logger.error(err);
            res.status(500).json({ message: 'Error while uploading' });
          });
    });


    const fileImportGroup = (yield getFileImportGroup(req.params.id, req.headers)).data.file_import_group;

    if (fileImportGroup && fileImportGroup.status !== 'open') {
      res.status(400).json({ message: 'Files cannot be uploaded to a non-open file import group.' });
      return;
    }

    req.pipe(busboy);
  })()
      .catch((err) => {
        logger.error(err);
        res.status(500).json({ message: 'Error while uploading' });
      });
};
