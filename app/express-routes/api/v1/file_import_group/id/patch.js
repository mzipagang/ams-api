const Promise = require('bluebird');
const config = require('../../../../../config');
const logger = require('../../../../../services/logger');
const DataWorker = require('../../../../../services/dataWorker');
const createSQSJobMessage = require('../../../../../services/createSQSJobMessage');

const SQSMessageApi = createSQSJobMessage.SQSMessageApi;
const querystring = require('querystring');
const asyncHandler = require('express-async-handler');
const AmsShared = require('ams-shared');

/**
 * @swagger
 * parameters:
 *   PatchFileImportGroup:
 *     name: body
 *     in: body
 *     description: Parts of File Import Group to update
 *     required: true
 *     schema:
 *       $ref: '#/definitions/FileImportGroup'
 */

/**
 * @swagger
 * /api/v1/file_import_group/{ID}:
 *   patch:
 *     description: Update a file import group
 *     produces:
 *       - application/json
 *     tags:
 *       - File Import Group
 *     security:
 *       - ApiKeyAuth: []
 *     parameters:
 *       - $ref: '#/parameters/PatchFileImportGroup'
 *       - name: ID
 *         in: path
 *         description: ID of File Import Group
 *         type: string
 *         required: true
 *     responses:
 *       200:
 *         description: Returns the updated file import group
 *         schema:
 *           type: object
 *           properties:
 *             data:
 *               properties:
 *                 file_import_group:
 *                   type: object
 *                   $ref: '#/definitions/FileImportGroup'
 */


class FileImportGroupUpdateController {
  static get FileImportSourceMap() {
    return {
      ...AmsShared.ams_models.FileImportSourceMap.PacModelsMap,
      ...config.FEATURE_FLAGS.DISABLE_SOURCE_FILES ? {} : AmsShared.ams_models.FileImportSourceMap.SourceFilesMap
    };
  }

  static forwardToDataWorker(req, headers) {
    return DataWorker(req.method, req.originalUrl, req.body, headers);
  }

  static async getPublishHistories(query) {
    const response = await DataWorker('get', `/api/v1/publish_history?${querystring.stringify(query)}`);
    return response.body.data.publish_histories || [];
  }

  static updatePublishHistory(publishHistory) {
    return DataWorker('patch', `/api/v1/publish_history/${publishHistory.id}`, publishHistory);
  }

  static getPublishableFiles(publishHistory) {
    const newFilesTypesMap = {};

    publishHistory.files.forEach((file) => {
      if (file.new_file) {
        newFilesTypesMap[FileImportGroupUpdateController.FileImportSourceMap[file.import_file_type]] = true;
      }
    });

    return publishHistory.files.filter((file) => {
      const currentFile = file;
      const type = FileImportGroupUpdateController.FileImportSourceMap[currentFile.import_file_type];

      if (newFilesTypesMap[type] || type.indexOf('pac') === 0) {
        currentFile.status = 'publishing';
        return true;
      }
      return false;
    });
  }

  static queuePublishMessages(publishableFiles, publishHistory, fileImportGroup) {
    // If no files - bypass publishing of each individual file
    if (publishableFiles.length === 0) {
      return [SQSMessageApi.create(1, 'PUBLISH_HISTORY_MERGE_FINISHED', {
        fileImportGroupId: fileImportGroup.id,
        publishHistoryId: publishHistory.id
      })];
    }

    return SQSMessageApi.create(1, 'PUBLISH_HISTORY_MERGE_FILE_QUEUE', {
      fileImportGroupId: fileImportGroup.id,
      publishHistoryId: publishHistory.id,
      fileIds: publishableFiles.map(file => file.file_id),
      groupType: publishHistory.group_type
    });
  }

  static async publishFiles(fileImportGroup) {
    const query = {
      file_import_group_id: fileImportGroup.id
    };

    const publishHistories = await FileImportGroupUpdateController.getPublishHistories(query);
    const updatePublishHistoryPromises = [];
    const queuePublishMessagesPromises = [];

    for (let i = 0; i < publishHistories.length; i += 1) {
      const publishHistory = publishHistories[i];

      const publishableFiles = FileImportGroupUpdateController.getPublishableFiles(publishHistory);

      updatePublishHistoryPromises.push(FileImportGroupUpdateController.updatePublishHistory(publishHistory));

      queuePublishMessagesPromises.push(
        FileImportGroupUpdateController.queuePublishMessages(publishableFiles, publishHistory, fileImportGroup)
      );
    }

    await Promise.all(updatePublishHistoryPromises);
    const result = await Promise.all(queuePublishMessagesPromises);
    return result;
  }

  static async updateStatus(req, res, next) {
    const headers = {};
    Object.keys(req.headers).filter(key => key === 'authorization' || key.indexOf('x-') === 0).forEach((key) => {
      headers[key] = req.headers[key];
    });

    try {
      const response = await FileImportGroupUpdateController.forwardToDataWorker(req, headers);
      const fileImportGroup = response.body.data.file_import_group;

      if (req.body.status === 'publish' && fileImportGroup.status === 'publishing') {
        await FileImportGroupUpdateController.publishFiles(fileImportGroup);
      }
      res.status(response.statusCode).json(response.body);
    } catch (err) {
      logger.error(err);
      next(err);
    }
  }
}

module.exports = asyncHandler(FileImportGroupUpdateController.updateStatus);
module.exports.FileImportGroupUpdateController = FileImportGroupUpdateController;
