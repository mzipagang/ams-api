const sessionCheckMiddleware = require('../../../../services/sessionCheckMiddleware');
// const ensurePermission = require('../../../../services/ensurePermission');

module.exports = (router) => {
  router.use(sessionCheckMiddleware);

  // In order to check permissions, use the ensurePermission middleware
  // permission check must always be after session check

  // router.post('/', ensurePermission('search'));

  return router;
};
