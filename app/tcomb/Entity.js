const t = require('tcomb');

const Address = t.struct({
  address_1: t.maybe(t.String),
  address_2: t.maybe(t.String),
  city: t.maybe(t.String),
  zip4: t.maybe(t.String),
  zip5: t.maybe(t.String),
  state: t.maybe(t.String)
}, 'Address');

const Entity = t.struct({
  apm_id: t.String,
  subdiv_id: t.maybe(t.String),
  id: t.String,
  start_date: t.Date,
  end_date: t.Date,
  name: t.String,
  dba: t.maybe(t.String),
  type: t.maybe(t.String),
  tin: t.maybe(t.String),
  npi: t.maybe(t.String),
  ccn: t.maybe(t.String),
  additional_information: t.maybe(t.String),
  address: t.maybe(Address)
}, 'Entity');

const PatchAddress = t.struct({
  address_1: t.maybe(t.String),
  address_2: t.maybe(t.String),
  city: t.maybe(t.String),
  zip4: t.maybe(t.String),
  zip5: t.maybe(t.String),
  state: t.maybe(t.String)
}, 'Address');

const PatchEntity = t.struct({
  apm_id: t.maybe(t.String),
  subdiv_id: t.maybe(t.String),
  id: t.maybe(t.String),
  start_date: t.maybe(t.Date),
  end_date: t.maybe(t.Date),
  name: t.maybe(t.String),
  dba: t.maybe(t.String),
  type: t.maybe(t.String),
  tin: t.maybe(t.String),
  npi: t.maybe(t.String),
  ccn: t.maybe(t.String),
  additional_information: t.maybe(t.String),
  address: t.maybe(PatchAddress)
}, 'PatchEntity');

module.exports = {
  Entity,
  PatchEntity
};
