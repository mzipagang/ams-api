const t = require('tcomb');

const YesNoPartialEnum = t.enums({
  N: 'No',
  Y: 'Yes',
  P: 'Partial'
}, 'YesNoPartialEnum');

const Model = t.struct({
  id: t.String,
  name: t.String,
  short_name: t.String,
  start_date: t.Date,
  end_date: t.maybe(t.Date),
  advanced_apm_flag: YesNoPartialEnum, // (N)o, (Y)es, (P)artial
  mips_special_scoring_flag: YesNoPartialEnum, // (N)o, (Y)es, (P)artial
  apm_quality_reporting_category: t.Integer
}, 'Model');

const PatchModel = t.struct({
  id: t.maybe(t.String),
  name: t.maybe(t.String),
  short_name: t.maybe(t.String),
  start_date: t.maybe(t.Date),
  end_date: t.maybe(t.Date),
  advanced_apm_flag: t.maybe(YesNoPartialEnum), // (N)o, (Y)es, (P)artial
  mips_special_scoring_flag: t.maybe(YesNoPartialEnum), // (N)o, (Y)es, (P)artial
  apm_quality_reporting_category: t.maybe(t.Integer)
}, 'PatchModel');

module.exports = {
  Model,
  PatchModel
};
