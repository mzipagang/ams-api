const t = require('tcomb');

const Provider = t.struct({
  entity_id: t.String,
  start_date: t.Date,
  end_date: t.Date,
  tin: t.String,
  npi: t.maybe(t.String),
  ccn: t.maybe(t.String),
  tin_type_code: t.maybe(t.String),
  specialty_code: t.maybe(t.String),
  organization_name: t.maybe(t.String),
  first_name: t.maybe(t.String),
  middle_name: t.maybe(t.String),
  last_name: t.maybe(t.String),
  participant_type_code: t.maybe(t.String)
}, 'Provider');

const PatchProvider = t.struct({
  entity_id: t.maybe(t.String),
  start_date: t.maybe(t.Date),
  end_date: t.maybe(t.Date),
  tin: t.maybe(t.String),
  npi: t.maybe(t.String),
  ccn: t.maybe(t.String),
  tin_type_code: t.maybe(t.String),
  specialty_code: t.maybe(t.String),
  organization_name: t.maybe(t.String),
  first_name: t.maybe(t.String),
  middle_name: t.maybe(t.String),
  last_name: t.maybe(t.String),
  participant_type_code: t.maybe(t.String)
}, 'PatchProvider');

module.exports = {
  Provider,
  PatchProvider
};
