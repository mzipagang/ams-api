const t = require('tcomb');

const FileImportGroup = t.struct({
  id: t.maybe(t.String),
  file_name: t.String,
  status: t.maybe(t.String),
  createdAt: t.maybe(t.Date),
  updatedAt: t.maybe(t.Date)
}, 'FileImportGroup');

const PatchFileImportGroup = t.struct({
  id: t.maybe(t.String),
  file_name: t.maybe(t.String),
  status: t.maybe(t.String),
  createdAt: t.maybe(t.Date),
  updatedAt: t.maybe(t.Date)
}, 'PatchFileImportGroup');

module.exports = {
  FileImportGroup,
  PatchFileImportGroup
};
