const t = require('tcomb');

const FileImportError = t.struct({
  file_import_id: t.String,
  error: t.String,
  line_number: t.Integer,
  createdAt: t.maybe(t.Date)
}, 'FileImportError');

const PatchFileImportError = t.struct({
  file_import_id: t.maybe(t.String),
  error: t.maybe(t.String),
  line_number: t.maybe(t.Integer),
  createdAt: t.maybe(t.Date)
}, 'PatchFileImportError');

module.exports = {
  FileImportError,
  PatchFileImportError
};
