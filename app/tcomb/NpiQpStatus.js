const t = require('tcomb');

const YesNoPartialEnum = t.enums({
  N: 'No',
  Y: 'Yes',
  P: 'Partial'
}, 'YesNoPartialEnum');

const ReportTypeEnum = t.enums({
  P: 'Predictive',
  I: 'Initial',
  S: 'Second',
  F: 'Final'
}, 'ReportTypeEnum');


const NpiQpStatus = t.struct({
  run_date: t.Date,
  qp_year: t.String,
  qp_reporting_period: ReportTypeEnum,
  run_number: t.Integer,
  npi: t.String,
  qp_status: YesNoPartialEnum
}, 'NpiQpStatus');

module.exports = {
  NpiQpStatus
};
