const tcomb = require('tcomb');
const config = require('../config');

module.exports = tcomb.enums.of([
  ...[
    'pac-model',
    'pac-entity',
    'pac-provider',
    'pac-subdivision',
    'pac-qp_status',
    'pac-qp_score',
    'pac-lvt'
  ],
  ...config.FEATURE_FLAGS.DISABLE_SOURCE_FILES ? [] : [
    'aco-entity',
    'mdm-provider',
    'ngaco-preferred-provider',
    'cmmi-entity',
    'cmmi-provider',
    'entity-eligibility',
    'individual_qp_threshold',
    'individual_qp_status',
    'entity_qp_threshold',
    'entity_qp_status'
  ]
], 'ImportFileType');
