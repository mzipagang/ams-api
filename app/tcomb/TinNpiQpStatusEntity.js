const t = require('tcomb');

const YesNoPartialEnum = t.enums({
  N: 'No',
  Y: 'Yes',
  P: 'Partial'
}, 'YesNoPartialEnum');

const ReportTypeEnum = t.enums({
  P: 'Predictive',
  I: 'Initial',
  S: 'Second',
  F: 'Final'
}, 'ReportTypeEnum');


const TinNpiQpStatusEntity = t.struct({
  apm_patient_count_denominator: t.Integer,
  apm_patient_count_numerator: t.Integer,
  apm_patient_threshold_met: YesNoPartialEnum,
  apm_patient_threshold_score: t.Number,
  apm_payment_denominator: t.Number,
  apm_payment_numerator: t.Number,
  apm_payment_threshold_met: YesNoPartialEnum,
  npi: t.String,
  entity_id: t.String,
  apm_id: t.String,
  qp_reporting_period: ReportTypeEnum,
  qp_status: YesNoPartialEnum,
  qp_year: t.String,
  run_date: t.Date,
  run_number: t.Integer,
  subdiv: t.String,
  tin: t.String
}, 'TinNpiQpStatusEntity');

module.exports = {
  TinNpiQpStatusEntity
};
