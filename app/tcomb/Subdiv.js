const t = require('tcomb');

const YesNoPartialEnum = t.enums({
  N: 'No',
  Y: 'Yes',
  P: 'Partial'
}, 'YesNoPartialEnum');

const Subdiv = t.struct({
  id: t.String,
  apm_id: t.String,
  name: t.String,
  start_date: t.Date,
  end_date: t.maybe(t.Date),
  advanced_apm_flag: YesNoPartialEnum
}, 'Subdiv');

const PatchSubdiv = t.struct({
  id: t.maybe(t.String),
  apm_id: t.maybe(t.String),
  name: t.maybe(t.String),
  start_date: t.maybe(t.Date),
  end_date: t.maybe(t.Date),
  advanced_apm_flag: t.maybe(YesNoPartialEnum)
}, 'Subdiv');

module.exports = {
  Subdiv,
  PatchSubdiv
};
