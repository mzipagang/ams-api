require('dotenv').config({ path: require('path').join(__dirname, '../.env') });

const moment = require('moment');
const config = require('./config');
const express = require('./services/express');
const logger = require('./services/logger');
const expressFileRouter = require('./services/expressFileRouter');

module.exports = {
  express: express.start(expressFileRouter.load())
      .then(() => {
        logger.info(`Server started at ${moment().format()} on port ${config.EXPRESS.PORT}`);
      })
      .catch((err) => {
        logger.error('Server failed to start:', err.message);
        logger.error(err);
      })
};
