# Developer Guide - API

Getting Started
---------

Prerequisites:
 - Node.js = 8.9, It is recommended that you use [NVM](https://github.com/creationix/nvm).
 - [Git](https://git-scm.com/)
 - Docker
 - Docker Compose

Clone the codebase and initialize it by running the following commands:
```shell
$ git clone https://github.cms.gov/CMMI/AMS-api
$ cd AMS-api
$ npm install
```

#### For development
Start up the docker development servers.
```shell
cd development
docker-compose up -d
```

Start up the app
```shell
npm run start:dev
```
Note: `start:dev` uses nodemon to automatically restart your node service when files change.


### For Testing
There are two utility commands to help with testing which allow for the importing/exporting of the current database. These scripts are not
sophisticated at the moment so there is some work on the user to ensure proper steps are taken.

In order to export a database run:

```
npm run db:export
```

This will create a gzip in the API application's root. The file's name is `mongo-backup.gz` and this cannot be changed. If you wish to save the name as something else or in a different location, you will need to move/copy the file yourself.

In order to import a database you must first have a file with the name `mongo-backup.gz` in the root of the API application. The name is non-negotaible as well as the location. After this has been completed, run:

```
npm run db:import
```

The database should now match what was saved in the archive file.

#### For production
```shell
export PORT=4000
export NODE_ENV=production
export ALLOWED_ORIGINS="http://ams-dev.internal.semanticbits.com"
npm run start
```


Environment variables
---------

Configuration of the app is based entirely on environment variables. The app does not store any variables for any environment at all. All configuration from environment variables is applied in `./app/config/index.js`.

| Environment Variable  | Default                                   | Options                                             |
| --------------------- | ----------------------------------------- | --------------------------------------------------- |
| NODE_ENV              | `"dev"`                                   | `production`, `dev`, or any other environment name  |
| PORT                  | `5000`                                    | any valid port number                               |
| ALLOWED_ORIGINS       | `""`                                      | comma separated HTTP ORIGIN list                    |


Tests
---------
```
npm run test
```
AMS-api is using Mocha to run tests. The test pattern is `test/**/*.spec.js` and is using Mocha's `bdd` interface. Feel free to use any Mocha runner to run the tests.


Lint
---------
```
npm run lint
```
The linting rules are based on Airbnb's Javascript Style Guide which can be found here: https://github.com/airbnb/javascript. Please run the linter before each commit.


Code Coverage
---------
```
npm run coverage:dev
```
`npm run coverage:dev` will automatically open the generated HTML file in your browser. For CI, run `npm run coverage` to generate but not open. The generated docs will be available at `./coverage/lcov-report/index.html`.


Swagger UI
---------
Coming Soon - Currently accessible json at ./api/v1/swagger.json
A swagger ui will be added soon as well.


Additional Notes
---------

#### Accessing GitHub w/ 2FA
Because CMS GitHub requires 2FA, you will need to checkout the repository using HTTPS, your username, and a Personal Access Token. Go to Settings -> Personal Access Tokens to create a Personal Access Token with "repo" access. Use this password, along with your GitHub username to access the repo. [More Info](https://help.github.com/articles/providing-your-2fa-authentication-code/#when-youll-be-asked-for-a-personal-access-token-as-a-password)
