FROM centos:7

RUN yum -y install epel-release
RUN yum -y install make which npm

RUN npm install -g n
RUN n 8.11.2

RUN mkdir -p /var/core/app /var/log/app

COPY . /var/core/app

WORKDIR /var/core/app
RUN npm rebuild

EXPOSE 8080
ENV PORT=8080
CMD [ "npm", "start" ]